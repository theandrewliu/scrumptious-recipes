from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings


USER_MODEL = settings.AUTH_USER_MODEL
# Create your models here.
class Mealplan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateField()
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="mealplans",
        on_delete=models.CASCADE,
        null=True,
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="mealplans")
