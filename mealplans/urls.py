from django.urls import path
from django.contrib.auth import views as auth_views

from mealplans.views import (
    MealplanCreateView,
    MealplanDeleteView,
    MealplanUpdateView,
    MealplanDetailView,
    MealplanListView,
)


urlpatterns = [
    path("", MealplanListView.as_view(), name="mealplans_list"),
    path("<int:pk>/", MealplanDetailView.as_view(), name="mealplan_detail"),
    path(
        "<int:pk>/delete/", MealplanDeleteView.as_view(), name="mealplan_delete"
    ),
    path("create/", MealplanCreateView.as_view(), name="mealplan_new"),
    path("<int:pk>edit/", MealplanUpdateView.as_view(), name="mealplan_edit"),
    path("accounts/login/", auth_views.LoginView.as_view(), name="login"),
    path("accounts/logout/", auth_views.LogoutView.as_view(), name="logout"),
]
