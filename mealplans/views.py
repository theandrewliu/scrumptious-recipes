from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from django.contrib.auth.mixins import LoginRequiredMixin

from mealplans.models import Mealplan

# Create your views here.


class MealplanListView(LoginRequiredMixin, ListView):
    model = Mealplan
    template_name = "mealplans/list.html"
    paginate_by = 3

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)


class MealplanDetailView(DetailView):
    model = Mealplan
    template_name = "mealplans/detail.html"

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)


class MealplanCreateView(LoginRequiredMixin, CreateView):
    model = Mealplan
    template_name = "mealplans/new.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("mealplans_list")

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("mealplan_detail", pk=plan.id)


class MealplanUpdateView(LoginRequiredMixin, UpdateView):
    model = Mealplan
    template_name = "mealplans/edit.html"
    fields = ["name", "date", "recipes"]

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("mealplan_detail", args=[self.object.id])


class MealplanDeleteView(LoginRequiredMixin, DeleteView):
    model = Mealplan
    template_name = "mealplans/delete.html"
    success_url = reverse_lazy("mealplans_list")

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)
